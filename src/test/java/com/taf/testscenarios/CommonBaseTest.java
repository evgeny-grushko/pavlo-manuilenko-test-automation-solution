package com.taf.testscenarios;

import com.taf.utils.loggers.TAFLogger;
import lombok.Getter;

public class CommonBaseTest {
    @Getter
    private static int countChildClassStarted = 0;
    private static int countPrintPerTestSession = 0;

    public CommonBaseTest() {
        countChildClassStarted++;
        if (countPrintPerTestSession++ <= 0) {
            TAFLogger.logFrameworkStart();
        }
    }
}
