package com.taf.testscenarios.ui.main;

import com.taf.testscenarios.ui.BaseUITest;
import org.assertj.core.api.Assertions;
import org.testng.annotations.Test;

import java.util.List;

import static com.taf.constants.UIConst.BASE_UI_URL;

public class MainPageTest extends BaseUITest {

    @Test(priority = 0,
            suiteName = "MainPageTests",
            testName = "Google Home page address test",
            description = "Requirements: Task_01",
            groups = "Regression")
    public void addressTest() {
        taf.openMainPage();
        Assertions.assertThat(taf.getCurrentUrl()).isEqualTo(BASE_UI_URL);
    }

    @Test(priority = 1,
            suiteName = "MainPageTests",
            testName = "Google Home page title text test",
            description = "Requirements: Task_02",
            groups = "Regression")
    public void titleTextTest() {
        taf.openMainPage();
        Assertions.assertThat(taf.getMainPageTitleText()).isEqualTo("Google");
    }

    @Test(priority = 2,
            suiteName = "MainPageTests",
            testName = "Google Home page Logo test",
            description = "Requirements: Task_03",
            groups = {"Regression", "MobileView"})
    public void googleLogoTest() {
        taf.openMainPage();
        Assertions.assertThat(taf.isGoogleLogoPresentOnMainPage()).isTrue();
    }

    @Test(suiteName = "MainPageTests",
            testName = "Google search field available test",
            description = "Requirements: Task_04",
            groups = "Regression",
            dependsOnMethods = {"googleLogoTest"})
    public void googleSearchFieldAvailableTest() {
        taf.openMainPage();
        Assertions.assertThat(taf.isGoogleSearchFieldPresentOnMainPage()).isTrue();
    }

    @Test(suiteName = "MainPageTests",
            testName = "Google Home page footer elements count test",
            description = "Requirements: Task_05",
            groups = "XBrowser",
            dependsOnMethods = {"googleLogoTest"})
    public void googleFooterElementsCountTest() {
        taf.openMainPage();
        Assertions.assertThat(taf.getMainPageFooterElementsCount()).isEqualTo(6);
    }

    @Test(testName = "Google Home page footer elements names test",
            description = "Requirements: Task_06",
            groups = "MobileView",
            dependsOnMethods = {"googleLogoTest"})
    public void googleFooterElementsNamesTest() {
        taf.openMainPage();
        List<String> expectedFooterElementsNames = taf.getMainPageFooterItems();
        List<String> actualFooterElementsNames = taf.getFooterElementsNamesOnMainPage();
        Assertions.assertThat(actualFooterElementsNames).isEqualTo(expectedFooterElementsNames);
    }

    @Test(testName = "Open About Google page from Home page footer test",
            description = "Requirements: Task_07",
            dependsOnMethods = {"googleLogoTest"})
    public void openAboutGooglePageFromMainPageFooterTest() {
        taf.openMainPage();
        taf.cleansBrowserCookies();
        taf.openAboutGooglePageFromMainPageFooter();
        Assertions.assertThat(taf.getAboutGooglePageExpURL()).isEqualTo(taf.getAboutGooglePageCurrentURL());
        Assertions.assertThat(taf.getAboutGooglePageTitleText()).isEqualTo(taf.getAboutGooglePageExpTitleText());
    }

}
