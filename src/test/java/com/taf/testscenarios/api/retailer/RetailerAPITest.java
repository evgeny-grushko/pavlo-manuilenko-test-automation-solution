package com.taf.testscenarios.api.retailer;

import com.taf.layers.api.models.retailer.RetailerData;
import com.taf.layers.api.models.retailer.RetailerShop;
import com.taf.testscenarios.api.MockedBaseAPITest;
import org.assertj.core.api.Assertions;
import org.testng.annotations.Test;

import java.io.File;

import static com.taf.constants.APIConst.*;
import static org.assertj.core.api.AssertionsForClassTypes.contentOf;

public class RetailerAPITest extends MockedBaseAPITest {

    @Test(suiteName = "RetailerAPITest",
            testName = "Verify id, location and name from single retailer info",
            description = "Requirements: Task_api-09",
            groups = "Regression")
    public void idAndNameSingleRetailerTest() {
        taf.getInterceptor().mockRetailerInfoEndpoint(RETAILER_ENDPOINT + "1");
        RetailerData retailerData = taf.getRestClient()
                .sendGetRequest(MOCK_API_URL + RETAILER_ENDPOINT + "1", CONTENT_TYPE_JSON)
                .getResponseAsObject("data", RetailerData.class);
        Assertions.assertThat(taf.getRestClient().getStatusCode()).isEqualTo(200);
        Assertions.assertThat(retailerData.getName()).isEqualTo("Smart Phones");
        Assertions.assertThat(retailerData.getLocation()).isEqualTo("Ukraine");
    }

    @Test(suiteName = "RetailerAPITest",
            testName = "Verify id and id in license are match from single retailer info",
            description = "Requirements: Task_api-09")
    public void idAndIdInShopLinkAreMatchForSingleRetailerTest() {
        taf.getInterceptor().mockRetailerInfoEndpoint(RETAILER_ENDPOINT + "1");
        RetailerData retailerData = taf.getRestClient()
                .sendGetRequest(MOCK_API_URL + RETAILER_ENDPOINT + "1", CONTENT_TYPE_JSON)
                .getResponseAsObject("data", RetailerData.class);
        Assertions.assertThat(taf.getRestClient().getStatusCode()).isEqualTo(200);
        Assertions.assertThat(retailerData.getIdFromLicense()).isEqualTo(retailerData.getId().toString());
    }

    @Test(suiteName = "RetailerAPITest",
            testName = "Get single retailer info - not found test (negative)",
            description = "Requirements: Task_api-09")
    public void singleRetailerNotFoundTest() {
        taf.getInterceptor().mockNonExistentRetailerInfoEndpoint(RETAILER_ENDPOINT + "20");
        taf.getRestClient().sendGetRequest(MOCK_API_URL + RETAILER_ENDPOINT + "20", CONTENT_TYPE_JSON);
        Assertions.assertThat(taf.getRestClient().getStatusCode()).isEqualTo(404);
        Assertions.assertThat(taf.getRestClient().getResponseBodyAsString())
                .isEqualTo(contentOf(new File(API_JSON_RESPONSES + "non_existent_retailer_info.json")));
    }

    @Test(suiteName = "RetailerAPITest",
            testName = "Verify id, name and address of main shop for a single retailer",
            description = "Requirements: Task_api-10",
            groups = "Regression")
    public void singleRetailerUpdateLocationTest() {
        taf.getInterceptor().mockRetailerShopInfoEndpoint(RETAILER_ENDPOINT + "1");
        RetailerShop retailerShop = taf.getRestClient()
                .setRequestParam("shop", "main")
                .sendGetRequest(MOCK_API_URL + RETAILER_ENDPOINT + "1", CONTENT_TYPE_JSON)
                .getResponseAsObject("shop_details", RetailerShop.class);
        Assertions.assertThat(taf.getRestClient().getStatusCode()).isEqualTo(200);
        Assertions.assertThat(retailerShop.getId().toString()).hasToString("1011");
        Assertions.assertThat(retailerShop.getName()).isEqualTo("Brand new phones");
        Assertions.assertThat(retailerShop.getAddress()).isEqualTo("Downing Street, 5");
    }

    @Test(suiteName = "RetailerAPITest",
            testName = "Verify retailer delete via id is successful",
            description = "Requirements: Task_api-11")
    public void deleteSingleUserTest() {
        taf.getInterceptor().mockRetailerDeleteEndpoint(RETAILER_ENDPOINT + "1");
        taf.getRestClient().sendDeleteRequest(MOCK_API_URL + RETAILER_ENDPOINT + "1");
        Assertions.assertThat(taf.getRestClient().getStatusCode()).isEqualTo(200);
        Assertions.assertThat(taf.getRestClient().getResponseBodyAsString())
                .isEqualTo(contentOf(new File(API_JSON_RESPONSES + "retailer_delete.json")));
    }

}
