package com.taf.utils.listeners;

import com.aventstack.extentreports.Status;
import com.codeborne.selenide.Selenide;
import com.taf.configuration.extentreports.ExtentReportsManager;
import com.taf.testscenarios.CommonBaseTest;
import lombok.NonNull;
import org.openqa.selenium.OutputType;
import org.testng.ITestContext;
import org.testng.ITestListener;
import org.testng.ITestResult;

import static com.taf.configuration.extentreports.ExtentReportsManager.getTest;
import static com.taf.utils.loggers.TAFLogger.*;

public class ExtentReportTestListener implements ITestListener {
    private static int passedTests = 0;
    private static int failedTests = 0;
    private static int skippedTests = 0;
    private static int countClassFinished = 0;

    private static String getTestMethodName(@NonNull ITestResult iTestResult) {
        return iTestResult.getMethod().getConstructorOrMethod().getName();
    }

    @Override
    public void onStart(@NonNull ITestContext iTestContext) {
        logClassOnStart(iTestContext.getName());
    }

    @Override
    public void onFinish(@NonNull ITestContext iTestContext) {
        logClassOnFinish(iTestContext.getName());
        if (CommonBaseTest.getCountChildClassStarted() == ++countClassFinished) {
            logTotalScenariosStatistic(passedTests, failedTests, skippedTests);
        }
        ExtentReportsManager.extentReports.flush();
    }

    @Override
    public void onTestStart(@NonNull ITestResult iTestResult) {
        logTestMethodStart(getTestMethodName(iTestResult));
    }

    @Override
    public void onTestSuccess(@NonNull ITestResult iTestResult) {
        passedTests++;
        logTestMethodPassed(getTestMethodName(iTestResult));
        getTest().log(Status.PASS, "PASSED");
    }

    @Override
    public void onTestFailure(@NonNull ITestResult iTestResult) {
        failedTests++;
        logTestMethodFailed(getTestMethodName(iTestResult), iTestResult.getThrowable());
        if (iTestResult.getTestClass().getRealClass().getPackageName().contains(".api.")) {
            getTest().log(Status.FAIL, iTestResult.getThrowable());
        } else {
            getTest().log(Status.FAIL, "FAILED", iTestResult.getThrowable(),
                    getTest().addScreenCaptureFromBase64String(Selenide.screenshot(OutputType.BASE64)).getModel().getMedia().get(0));
        }
    }

    @Override
    public void onTestSkipped(@NonNull ITestResult iTestResult) {
        skippedTests++;
        logTestMethodSkipped(getTestMethodName(iTestResult));
        getTest().log(Status.SKIP, "SKIPPED");
    }

}
