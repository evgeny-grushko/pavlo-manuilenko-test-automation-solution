package com.taf.utils.loggers;

import lombok.extern.log4j.Log4j2;

import static com.taf.configuration.TAFConfig.getSystemProperty;

@Log4j2
public class TAFLogger {

    public static void logFrameworkStart() {
        log.info("======================Test Automation Framework is running======================");
    }

    public static void logClassOnStart(String className) {
        log.info("[{}] class is onStart() method.", className);
    }

    public static void logClassOnFinish(String className) {
        log.info("[{}] class is onFinish() method.", className);
    }

    public static void logTestMethodStart(String testMethodName) {
        log.info("{} is starting...", testMethodName);
    }

    public static void logTestMethodPassed(String testMethodName) {
        log.info("{} is PASSED", testMethodName);
    }

    public static void logTestMethodFailed(String testMethodName, Throwable throwable) {
        log.error("{} is FAILED!\n{}", testMethodName, throwable);
    }

    public static void logTestMethodSkipped(String testMethodName) {
        log.warn("{} is SKIPPED", testMethodName);
    }

    public static void logTotalScenariosStatistic(int passedTests, int failedTests, int skippedTests) {
        log.info("\n===============================================" +
                        "\nTest Suite ({}.xml) is completed!" +
                        "\nTotal tests run: [{}], Passed: [{}], Failed: [{}], Skipped: [{}]{}",
                getSystemProperty("testSuiteName", "all_tests"),
                (passedTests + failedTests + skippedTests),
                passedTests,
                failedTests,
                skippedTests,
                "\n===============================================\n");
    }
}
