/**
 * Server interceptor calls class used WireMockServer for different mock endpoint
 * Class-Based Singleton (private constructor, static field containing its only instance, static method for obtaining the instance)
 */
package com.taf.layers.api;

import com.github.tomakehurst.wiremock.WireMockServer;
import com.github.tomakehurst.wiremock.common.ConsoleNotifier;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;

import static com.github.tomakehurst.wiremock.client.WireMock.*;
import static com.github.tomakehurst.wiremock.core.WireMockConfiguration.wireMockConfig;

@NoArgsConstructor(access = AccessLevel.PRIVATE)
public final class ServerInterceptorCalls {
    private static final ServerInterceptorCalls INSTANCE = new ServerInterceptorCalls();
    private static final String CONNECTION = "Connection";
    private static final String KEEP_ALIVE = "keep-alive";
    private static final String CONTENT_TYPE = "Content-Type";
    private static final String JSON = "application/json";
    private static final String ROOT_PACKAGE = "interceptor_json_responses/";
    private static final WireMockServer wireMockServer =
            new WireMockServer(wireMockConfig().notifier(new ConsoleNotifier(false)));//Will start on port 8080, no HTTPS by default

    public static synchronized ServerInterceptorCalls getInstance() {
        return INSTANCE;
    }

    public void start() {
        if (!wireMockServer.isRunning()) {
            wireMockServer.start();
        }
    }

    public void stop() {
        if (wireMockServer.isRunning()) {
            wireMockServer.stop();
        }
    }

    public void mockRetailerInfoEndpoint(String endpoint) {
        stubFor(get("/" + endpoint)
                .willReturn(ok()
                        .withHeader(CONNECTION, KEEP_ALIVE)
                        .withHeader(CONTENT_TYPE, JSON)
                        .withBodyFile(ROOT_PACKAGE + "retailer_info.json")));
    }

    public void mockNonExistentRetailerInfoEndpoint(String endpoint) {
        stubFor(get("/" + endpoint)
                .willReturn(notFound()
                        .withHeader(CONNECTION, KEEP_ALIVE)
                        .withHeader(CONTENT_TYPE, JSON)
                        .withBodyFile(ROOT_PACKAGE + "non_existent_retailer_info.json")));
    }

    public void mockRetailerShopInfoEndpoint(String endpoint) {
        stubFor(get("/" + endpoint + "?shop=main")
                .willReturn(ok()
                        .withHeader(CONNECTION, KEEP_ALIVE)
                        .withHeader(CONTENT_TYPE, JSON)
                        .withBodyFile(ROOT_PACKAGE + "retailer_shop_info.json")));
    }

    public void mockRetailerDeleteEndpoint(String endpoint) {
        stubFor(delete("/" + endpoint)
                .willReturn(ok()
                        .withHeader(CONNECTION, KEEP_ALIVE)
                        .withHeader(CONTENT_TYPE, JSON)
                        .withBodyFile(ROOT_PACKAGE + "retailer_delete.json")));
    }
}
