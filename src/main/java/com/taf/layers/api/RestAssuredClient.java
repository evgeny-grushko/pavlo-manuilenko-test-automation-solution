/**
 * REST Assured facade class for API testing
 * Not determinative, public methods return the object, because the Method Chaining pattern is applied
 */
package com.taf.layers.api;

import io.restassured.response.Response;
import io.restassured.response.ResponseBodyExtractionOptions;
import io.restassured.specification.RequestSpecification;
import lombok.AccessLevel;
import lombok.NoArgsConstructor;
import lombok.NonNull;

import java.util.List;
import java.util.Map;

import static com.taf.constants.APIConst.CONTENT_TYPE_JSON;
import static io.restassured.RestAssured.given;
import static io.restassured.http.ContentType.fromContentType;

@NoArgsConstructor(access = AccessLevel.PUBLIC)
public class RestAssuredClient {
    private Response response;
    private RequestSpecification requestOptions = given();

    public RestAssuredClient setRequestParam(String key, String value) {
        requestOptions = requestOptions.queryParam(key, value);
        return this;
    }

    public RestAssuredClient setRequestParams(@NonNull Map<String, String> queryParams) {
        requestOptions = requestOptions.queryParams(queryParams);
        return this;
    }

    public RestAssuredClient setRequestHeader(String key, String value) {
        requestOptions = requestOptions.header(key, value);
        return this;
    }

    public RestAssuredClient setRequestHeaders(@NonNull Map<String, String> headers) {
        requestOptions = requestOptions.headers(headers);
        return this;
    }

    public RestAssuredClient sendGetRequest(String url, @NonNull String contentType) {
        response = requestOptions
                .log().uri()
                .expect()
                .contentType(fromContentType(contentType))
                .when()
                .get(url);
        requestOptions = given();
        return this;
    }

    public RestAssuredClient sendPostRequest(String url, @NonNull String contentType, Object obj) {
        response = requestOptions
                .contentType(fromContentType(contentType))
                .when().log().uri()
                .body(obj)
                .post(url);
        requestOptions = given();
        return this;
    }

    public RestAssuredClient sendPutRequest(String url, @NonNull String contentType, Object obj) {
        response = requestOptions
                .contentType(fromContentType(contentType))
                .when().log().uri()
                .body(obj)
                .put(url);
        requestOptions = given();
        return this;
    }

    public RestAssuredClient sendPatchRequest(String url, @NonNull String contentType, Object obj) {
        response = requestOptions
                .contentType(fromContentType(contentType))
                .when().log().uri()
                .body(obj)
                .patch(url);
        requestOptions = given();
        return this;
    }

    public RestAssuredClient sendDeleteRequest(String url) {
        response = requestOptions
                .contentType(fromContentType(CONTENT_TYPE_JSON))
                .when().log().uri()
                .delete(url);
        requestOptions = given();
        return this;
    }

    public int getStatusCode() {
        return response.statusCode();
    }

    public ResponseBodyExtractionOptions getResponseBody() {
        return response
                .then()
                .extract()
                .body();
    }

    public String getResponseBodyAsString() {
        return response
                .then()
                .extract()
                .body()
                .asString();
    }

    public <T> T getResponseAsObject(String jsonPath, Class<T> obj) {
        return response
                .then()
                .extract()
                .body()
                .jsonPath()
                .getObject(jsonPath, obj);
    }

    public <T> List<T> getResponseAsObjectsList(String jsonPath, Class<T> obj) {
        return response
                .then()
                .extract()
                .body()
                .jsonPath()
                .getList(jsonPath, obj);
    }

}
