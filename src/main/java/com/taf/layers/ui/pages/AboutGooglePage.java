package com.taf.layers.ui.pages;

import com.codeborne.selenide.Condition;
import com.codeborne.selenide.SelenideElement;
import org.openqa.selenium.By;

import java.time.Duration;

import static com.codeborne.selenide.Selenide.$;
import static com.codeborne.selenide.Selenide.open;
import static com.codeborne.selenide.WebDriverRunner.url;

public class AboutGooglePage extends BasePage {
    public static final String EXP_PAGE_URL = "https://about.google/";
    public static final String EXP_TITLE_TEXT = "Google - About Google, Our Culture & Company News";
    public static final String EXP_HEADER_TEXT = "Our mission is to \n the world’s \n and make it \n and \n.";
    private static final String HEADER_LOCATOR = "//h1[contains(@class, 'modules-lib__mission-statement')]";

    public AboutGooglePage() {
        waitUntilPageIsLoaded();
    }

    public AboutGooglePage(String aboutGooglePageAddress) {
        open(aboutGooglePageAddress);
        waitUntilPageIsLoaded();
    }

    public SelenideElement getTitle() {
        return $(By.xpath("//title[text()]"));
    }

    @Override
    public boolean waitUntilPageIsLoaded() {
        super.waitUntilPageIsLoaded();
        return $(By.xpath(HEADER_LOCATOR)).should(Condition.visible, Duration.ofSeconds(PAGE_LOAD_TIMEOUT_IN_SECONDS)).isDisplayed();
    }

    public SelenideElement getHeaderFromPage() {
        return $(By.xpath(HEADER_LOCATOR));
    }

    @Override
    public String getCurrentUrl() {
        return url().substring(0, url().indexOf('?'));
    }
}
