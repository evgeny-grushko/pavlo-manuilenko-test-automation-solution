# TestAutomationSolution
This automation testing framework (TAF) is created and maintained by [Pavlo Manuilenko](https://www.linkedin.com/in/manuilenko-pavlo/)

The main purpose of this TAF is to use it as a base (skeleton), which saves a lot of time at the beginning of the project and helps to get the AT up and running quicker.

## Applied technologies (Frameworks)
Java 11, Maven, Selenide (Selenium Webdriver), TestNG, AssertJ, REST Assured, WireMock, Log4J2, Extent Reports, Lombok.<hr/>

## Table of contents
>1. [Initialization of the TAF](#initialization-of-the-taf)
><br/>1.1 [Required Software, Tools and Prerequisites](#required-software-tools-and-prerequisites)
><br/>1.2 [How to build maven projects and setup different test runs](#how-to-build-maven-projects-and-setup-different-test-runs)
><br/>1.3 [Local run and debugging](#local-run-and-debugging)
>2. [API testing](#api-testing)
><br/>2.1 [Mock API calls](#mock-api-calls)
>3. [UI testing](#ui-testing)
><br/>3.1 [Cross-browser testing](#cross-browser-testing)
><br/>3.2 [Mobile-view testing](#mobile-view-testing)
>4. [Reporting](#reporting)
>5. [CI/CD](#cicd)
<hr />

<a name="initialization-of-the-taf"></a>
### Initialization of the TAF
This TAF is as a base (skeleton) of usual WEB automation testing.
If it's architecture and technologies meet your primary needs just clone (`git clone https://gitlab.com/pavlo.manuilenko/testautomationsolution.git`) 
the repo and adapt it under your project (**recommended**).<br>[Fork project](https://gitlab.com/pavlo.manuilenko/testautomationsolution/-/forks/new) 
is another way to use this TAF for your needs, which allows you to make changes without affecting the original project.<br>

There are some examples of page objects and components under `src/main/java/com/taf/layers/ui`, API models under `src/main/java/com/taf/layers/api`
and tests under `src/test/java/com/taf/testscenarios` which should be removed during the adaptation (except base classes).

Also, there is one separate feature brunch which implements BDD approach ([origin/BDD_style](https://gitlab.com/pavlo.manuilenko/testautomationsolution/-/tree/BDD_style)) and supporting the same TAF functionalities and volume.<br>
Merge `BDD_style` brunch into `master` in case you need to use behavior-driven development approach in the automation testing or delete `BDD_style` brunch if you don't need it.

<a name="required-software-tools-and-prerequisites"></a>
### Required Software, Tools and Prerequisites

* **Java** version: **Oracle Java 11** and higher (Execute `java -version` in command line after installation)
* **Apache Maven** version: **3.9+** and higher (Execute `mvn -version` in command line after installation)
* Latest version of Google **Chrome** and / or another Browsers

 <a name="how-to-build-maven-projects-and-setup-different-tests-run"></a>
### How to build maven projects and setup different test runs

* Open a terminal or command prompt
* Go to project's root
* Execute `mvn install` if you just want to run all test suites and don't want to care about anything else
* Execute `mvn clean test -DtestSuiteName=all_tests` to explicitly specify that all test suites run
* Execute `mvn clean test -DtestSuiteName=regression_tests` in order to run full regression suite
* In order to run tests in parallel mode set appropriate thread-count in command-line parameter `mvn clean test -DthreadCount=3`
* _Example: you need to run all UI-tests in MS Edge in parallel (2 threads) and headless mode, using local env-parameters:_<br/>
`mvn clean test -DtestSuiteName=all_ui_tests -DthreadCount=2 -Dui.browser=edge -Dheadless=true -Denv=local`
  
<a name="local-run-and-debugging"></a>
### Local run and debugging
There are three ways to run tests locally:
1) Using [▶] (Run Test button) directly above test method or test class (example: `src/test/java/com/taf/testscenarios/api/user/UserAPITest.java`)
2) Using one of XML TestNG suite from suites directory (example: `src/main/resources/suites/all_tests.xml`)
3) Using Maven command (example: `mvn clean install`)

Recommend to using the latest version of IntelliJ IDEA Community Edition with next plugins: SonarLint, Lombok, TestNG, CSV Plugin, etc.<br/>
There is a known issue ([IDEA-73260](https://youtrack.jetbrains.com/issue/IDEA-73260)) with showing test structure in parallel mode in IntelliJ IDEA Run console (that affects only view in local runs),
but the IDE-run executing in one thread by default. Add `parallel` type and `threadCount` parameters in the `suite` tag of an appropriate xml-file for TestNG, or simply run via Maven using `-DthreadCount...` parameter.<hr />

<a name="api-testing"></a>
### API testing
API testing implemented through [REST Assured ](https://rest-assured.io/)framework.<br/>
Execute `mvn clean test -DtestSuiteName=all_api_tests` in order to run only all API tests

TAF class returning instance of RestAssured client calling method `getRestClient()`.

Method Chaining pattern is applied which means that calling different methods in a single line instead of calling other 
methods with the same object reference separately. Under this pattern, we have to write the object reference once and
then call the methods by separating them with a dot.

<a name="mock-api-calls"></a>
### Mock API calls
If you need to develop API tests on endpoints that are still under development and are not available, but the contracts are known,
then you can start writing and debugging tests using a request interception server.<br>ServerInterceptorCalls class uses [WireMockServer](https://wiremock.org/) framework.<br>
**How to mock API test**: just extends from MockedBaseAPITest class in your test class and add required mock endpoint method, which should be called before sending a request <br/>
(***important note**: request URL have to contain address to localhost instead of real base URL or only endpoint part).<hr />

<a name="ui-testing"></a>
### UI testing
WEB UI testing is occurs through [Selenide](https://selenide.org/) framework which powered by [Selenium WebDriver](https://www.selenium.dev/documentation/webdriver/).<br/>
Execute `mvn clean test -DtestSuiteName=all_ui_tests` in order to run only all WEB UI tests in primary (default) browser.

<a name="cross-browser-testing"></a>
#### Cross-browser testing
Collect the browser statistic of the SUT to define the required scope of browsers, or just use [Global Browser Stats](https://gs.statcounter.com/) (maybe one primary browser is enough in the case).
<br/>If XBrowser Testing is required this TAS is supported next browsers: 
- Google **Chrome** (default), using Blink engine;
- Microsoft **Edge**, using Blink engine;
- Mozilla **Firefox**, using Gecko engine;
- Apple **Safari**, using WebKit engine (testing is only makes sense if the TAS is running under the macOS, because Apple only supports browser updates for its OS).

>This TAS skeleton prepared to realize both strategy of XBrowser Testing:
>- **The first**: All UI tests are XBrowser, so each UI test runs multiple times for each browser from the defined scope.
>- **The second**: All tests are divided into two types: first is a common type and that tests need to be run only once per
testing session (in primary browser), and the second type of tests (prone to Cross-browser bugs) which need to be run multiple times for each browser from the defined scope.
>
>>An example of how possible to build CI/CD process in case of the **first XBrowser Testing strategy**:<br/>
>>Setup sequential jobs or stages of pipeline, for example the first run can be `mvn clean test -DtestSuiteName=all_api_tests` (it will run all API tests),<br/>
then `mvn test -DtestSuiteName=all_ui_tests -Dui.browser=chrome` (it will run all UI tests in Chrome (possible to skip browser parameter in this case)),<br/>
then `mvn test -DtestSuiteName=all_ui_tests -Dui.browser=safari` (it will run all UI tests in Safari),<br/>
then `mvn test -DtestSuiteName=all_ui_tests -Dui.browser=edge` (it will run all UI tests in Edge),<br/>
after that `mvn test -DtestSuiteName=all_ui_tests -Dui.browser=firefox` (it will run all UI tests in Firefox).
>
>>In case of the **second XBrowser Testing strategy** necessary to define `groups = "XBrowser"` in parameter of `@Test` for all UI tests which need to be run multiple times for each browser from the defined scope.
<br/>Then possible to build CI/CD process somehow like this:<br/>
>>Setup job or pipeline using the next command: `mvn clean test -DtestSuiteName=all_api_tests` (it will run all API tests),<br/>
    then `mvn test -DtestSuiteName=all_ui_tests` (it will run all UI tests in Chrome),<br/>
    then `mvn test -DtestSuiteName=cross_browser_tests -Dui.browser=safari` (it will run all UI tests which marked "XBrowser" in Safari),<br/>
    then `mvn test -DtestSuiteName=cross_browser_tests -Dui.browser=edge` (it will run all UI tests which marked "XBrowser" in Edge),<br/>
    and finally `mvn test -DtestSuiteName=cross_browser_tests -Dui.browser=firefox` (it will run all UI tests which marked "XBrowser" in Firefox),<br/>

<a name="mobile-view-testing"></a>
#### Mobile-view testing
* Execute `mvn clean test -DtestSuiteName=mobile_view_tests` in order to run all the UI tests that are marked to test in **360x800** mobile view.<br/>
The size of the browser will take from the UI constants class (`MOBILE_BROWSER_SIZE`) that receives a value from TAF Properties (`ui.mobileBrowserSize`)<br/>
It's necessary to define `groups = "MobileView"` in parameter of `@Test` for all UI tests which need to be run in a mobile view.
<hr />

<a name="reporting"></a>
### Reporting 

* **[Extent Reports](https://www.extentreports.com/)** framework is applied.

The report is generated after all the tests run in **Report.html**, and the location path is taken from the TAF properties.<br/>
**Project name** and the **system under test** used in the reports also taken from TAF properties.

To include the test to the Report you need to call the appropriate method for each test-method or placed it into `@BeforeMethod`.<br/><br/>
`@Test` annotation important parameters:
1. [x] `suiteName` parameter is used for grouping tests under a category or if this parameter is absent tests will group under "Other" category in the report.
2. [x] `testName` parameter is used for identify the test. If this parameter is absent test will get `[Absent name]`.
3. [x] `description` parameter is used for additional information (requirements links, tasks, id, etc.) and present in detail-headers of tests.
   
* To show method invocation in the report details necessary to call the appropriate method each time for each target (action, assert, etc.) method.<br/>
`showMethodInvocationInReportDetails` method takes boolean `showAsStep` to print in two possible styles (as method call or in human-readable style).<br/>
* Screenshots attached into details for failed UI tests in Base64.
<hr />

<a name="cicd"></a>
### CI/CD

Continuous Integration is implemented via [GitLab CI/CD](https://docs.gitlab.com/ee/ci/)

There is `.gitlab-ci.yml` in the project root directory, which describes pipeline with some common AT jobs:
 - Manual run AT job with flexible variables (variables dropdown with presets are available on GitLab -> CI/CD -> Run pipeline)
 - Regression tests suite job launched on merge requests
 - Daily night complete regression job (CI/CD schedule must be configured using cron and variable `REGRESSION_TYPE` with value `"daily"` on GitLab)
 - Weekly night full regression jobs with all API tests, All UI tests in default browser, cross browser tests and mobile view tests (CI/CD schedule must be configured using cron and variable `REGRESSION_TYPE` with value `"weekly"` on GitLab)

Setup and make registration of own [GitLab Runner](https://docs.gitlab.com/runner/#gitlab-runner) required to work with GitLab CI/CD<br>
If Docker containers will be using instead of real local environment (e.g. Windows PowerShell) necessary to add `image: maven:3.8.1-openjdk-11` in `.gitlab-ci.yml` (at the level of jobs or the entire file).

Job artifacts contains Report.html provided by Extent Reports, also `Running after_script` prints a direct link to the report in the job-log.